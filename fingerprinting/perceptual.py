u"""
Created on 10/07/18
by fccoelho
license: GPL V3 or Later
"""

from imagehash import average_hash, phash, dhash, whash
from PIL import Image
import networkx as nx
from glob import glob
import os
from itertools import combinations


def get_image_hashes(img_path: str) -> dict:
    """
    Returns the perceptual hashes of an image.
    :param img_path: path to the image to be hashed
    :return: hash value
    """
    im = Image.open(img_path)
    hashes = {}
    for hn, h in {'ahash': average_hash, 'phash': phash, 'dhash': dhash, 'whash': whash}.items():
        hashes[hn] = h(im)
    return hashes


def build_proximity_network(path: str, ftype: str = 'JPG'):
    """
    From a directory of images build a network in which the edges are weighted according to
    imagehash distances
    :param ftype: extension of images
    :param path: directory where imags are located
    :return:
    """
    Net = nx.Graph()
    hashes = {}
    os.chdir(path)
    for imname in glob('**/*.{}'.format(ftype), recursive=True):
        print(imname)
        im = Image.open(imname)
        hashes[imname] = phash(im)
    for a, b in combinations(hashes, 2):
        hamm_dist = hashes[a] - hashes[b]
        Net.add_edge(a, b, weight=hamm_dist)

    return Net

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    G = build_proximity_network('../dados/')
    # this generates the adjacency matrix of the graph, which is the hamming distance matrix
    print(nx.to_numpy_matrix(G))
    nx.draw(G)
    plt.axis('off')
    plt.show()
