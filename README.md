﻿# Fingerprinting de Documentos digitais
Neste projeto iremos treinar um modelo para detectar pequenas alterações em um documento de texto digitalizado de maneira automática.

# Buscar artigos sobre fingerprinting

## Alexandre Tessarollo

1. [https://link.springer.com/chapter/10.1007/3-540-45123-4_1](https://link.springer.com/chapter/10.1007/3-540-45123-4_1) - O artigo fala sobre similaridade (em inglês, resemblance) e como o autor utilizou este conceito para agrupar ou discernir grupos de documentos com conteúdos parecidos. Creio que, apesar de antigo (1998), o artigo pode contribuir ao elaborar este conceito de resemblance tanto na teoria como na prática - cabe ressaltar que o artigo ajudou a desenvolver melhorias no AltaVista, um predecessor do Google, numa época em um buscador como esses leva um mês para percorrrer 30 milhões de documentos na web.

## Brenda Prallon

1. [https://www.researchgate.net/publication/266644656_A_new_algorithm_for_color_image_comparison_based_on_similarity_measures](https://www.researchgate.net/publication/266644656_A_new_algorithm_for_color_image_comparison_based_on_similarity_measures) - O artigo fala sobre comparação de imagens coloridas (não é exatamente o nosso caso, mas parece que pode ser generalizado) baseado em "similarity measures" que segmentam a imagem para fazer a comparação.

2. [https://ieeexplore.ieee.org/abstract/document/6471224/](https://ieeexplore.ieee.org/abstract/document/6471224/) - Abstract: "In this paper, we present a new scene text detection algorithm based on two machine learning classifiers: one allows us to generate candidate word regions and the other filters out nontext ones. To be precise, we extract connected components (CCs) in images by using the maximally stable extremal region algorithm. These extracted CCs are partitioned into clusters so that we can generate candidate regions. Unlike conventional methods relying on heuristic rules in clustering, we train an AdaBoost classifier that determines the adjacency relationship and cluster CCs by using their pairwise relations. Then we normalize candidate word regions and determine whether each region contains text or not. Since the scale, skew, and color of each candidate can be estimated from CCs, we develop a text/nontext classifier for normalized images. This classifier is based on multilayer perceptrons and we can control recall and precision rates with a single free parameter. Finally, we extend our approach to exploit multichannel information. Experimental results on ICDAR 2005 and 2011 robust reading competition datasets show that our method yields the state-of-the-art performance both in speed and accuracy."

## Ana Carolina Wagner

1. [A Fingerprint Verification System Based on Triangular Matching and Dynamic Time Warping](https://ieeexplore.ieee.org/document/888711/) - Apesar de não ser o que queremos, uma lida no artigo é interessante para entender a dinâmica do sistema de verificação da impressão digital.

2. [FINGERPRINT RECOGNITION SYSTEM USING ARTIFICIAL NEURAL NETWORK AS FEATURE EXTRACTOR:
DESIGN AND PERFORMANCE EVALUATION](https://content.sciendo.com/view/journals/tmmp/67/1/article-p117.xml)

## Igor da Silva Carvalho

1. [https://books.google.com.br/books?hl=pt-PT&lr=&id=1Wpx25D8qOwC&oi=fnd&pg=PR11&dq=Fingerprint+Recognition,+papers&ots=9yM49Qpv92&sig=uGqduInGd20h1DAZjn1CxafLkM8#v=onepage&q&f=true](https://books.google.com.br/books?hl=pt-PT&lr=&id=1Wpx25D8qOwC&oi=fnd&pg=PR11&dq=Fingerprint+Recognition,+papers&ots=9yM49Qpv92&sig=uGqduInGd20h1DAZjn1CxafLkM8#v=onepage&q&f=true) - Livro: "Handbook of fingerprint recognition". Acredito que o capítulo 4(a partir da pagína 167), que fala sobre fingerprint matching, seja o mais interessante pois trata de grau de similaridade e identificação de imagens. 

# Tutoriais
Nesta seção temos uma coletânea de tutorias e bibliotecas interessantes para o assunto.

1. [image hashing with opencv and Python](https://www.pyimagesearch.com/2017/11/27/image-hashing-opencv-python/) - Blog descrevendo a implementação de image hashing apenas usando openCV e Python.

2. [Fingerprinting Images for Near-Duplicate Detection](https://realpython.com/fingerprinting-images-for-near-duplicate-detection/) - Tutorial usando a PIL e a biblioteca Imagehash do Python.

3. [Duplicate image detection with perceptual hashing in Python](http://tech.jetsetter.com/2017/03/21/duplicate-image-detection/)

4. [Image recognition and object detection with opencv](https://www.learnopencv.com/image-recognition-and-object-detection-part1/)

# Bibliografia
1. Meuschke, Norman, et al. ["An adaptive image-based plagiarism detection approach."](https://www.gipp.com/wp-content/papercite-data/pdf/meuschke2018.pdf) Proc. ACM/IEEE Joint Conf. on Digital Libraries (JCDL). 2018.

2. Nguyen, Dat Tien, et al. ["Automatic image filtering on social networks using deep learning and perceptual hashing during crises."](https://arxiv.org/abs/1704.02602) arXiv preprint arXiv:1704.02602 (2017).

3. Lei, Yanqiang, Yuangen Wang, and Jiwu Huang. ["Robust image hash in Radon transform domain for authentication."](https://www.sciencedirect.com/science/article/pii/S0923596511000452) Signal Processing: Image Communication 26.6 (2011): 280-288.

4. Haouzia, Adil, and Rita Noumeir. ["Methods for image authentication: a survey."](https://link.springer.com/article/10.1007/s11042-007-0154-3) Multimedia tools and applications 39.1 (2008): 1-46.

5. Ahmed, Fawad, Mohammed Yakoob Siyal, and Vali Uddin Abbas. ["A secure and robust hash-based scheme for image authentication."](https://www.sciencedirect.com/science/article/pii/S0165168409002497) Signal Processing 90.5 (2010): 1456-1470.

6. Paternain, Daniel & Galar, Mikel & Jurio, Aranzazu & Barrenechea, Edurne. (2013). ["A new algorithm for color image comparison based on similarity measures"](https://www.researchgate.net/publication/266644656_A_new_algorithm_for_color_image_comparison_based_on_similarity_measures). 10.2991/eusflat.2013.88. 

7. H. I. Koo and D. H. Kim, ["Scene Text Detection via Connected Component Clustering and Nontext Filtering"](https://ieeexplore.ieee.org/abstract/document/6471224/). IEEE Transactions on Image Processing, vol. 22, no. 6, pp. 2296-2305, June 2013.

# Tamanho das imagens
Vimos que o formato mais adequado para reduzir o tamanho dos arquivos era .JPG (https://www.scantips.com/basics09.html) (https://www.digitaltrends.com/photography/jpeg-vs-png/)
Além disso, por meio de tentativa e erro decidimos que a dimensão das imagens deveria ser de 576x768 pixels, de modo a parmanecerem legíveis. Os comandos usados nesse teste e na para a padronização das imagens foram do pacote "imagemagick", na própria linha de comando do Linux:
```
mogrify -rotate "90>" *.JPG
mogrify -orient top-left *.JPG
mogrify -resize 576x768! *.JPG
```